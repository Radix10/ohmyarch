let mapleader = " "

syntax enable
set cursorline
set ttimeoutlen=100
set nu rnu
set termguicolors
set background=light
set expandtab
set ts=4

nmap <leader>w :w<CR>
nmap <leader>q :q<CR>
nmap <leader>e :e<CR>
nmap <leader>v "+gp
vmap <leader>c "+y

" add support for jsonc comment
autocmd FileType json syntax match Comment +\/\/.\+$+

" gruvbox
colorscheme gruvbox
let g:gruvbox_italic = 1
let g:gruvbox_italicize_strings = 1

" lightline
let g:lightline = {
      \ 'colorscheme': 'gruvbox',
      \ }

" nerdtree
map <C-n> :NERDTreeToggle<CR>

" tagbar
map <C-t> :Tagbar<CR>

"nvim-lsp
" LSP settings
:lua << EOF
  require'nvim_lsp'.pyls.setup{}
EOF

autocmd Filetype python setlocal omnifunc=v:lua.vim.lsp.omnifunc
nnoremap <silent> gd        <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> <c-]>     <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> K         <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> gD        <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> <c-k>     <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> 1gD       <cmd>lua vim.lsp.buf.type_definition()<CR>
nnoremap <silent> gr        <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> g0        <cmd>lua vim.lsp.buf.document_symbol()<CR>
nnoremap <silent> <Leader>n <cmd>lua vim.lsp.buf.rename()<CR>
