call plug#begin(stdpath('data') . '/plugged')
Plug 'lilydjwg/fcitx.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'jiangmiao/auto-pairs'
Plug 'scrooloose/nerdtree'
Plug 'morhetz/gruvbox'
Plug 'majutsushi/tagbar'
Plug 'junegunn/fzf.vim'
Plug 'aklt/plantuml-syntax'
Plug 'itchyny/lightline.vim'
Plug 'neovim/nvim-lsp'
Plug 'BrandonRoehl/auto-omni'
call plug#end()
