autoload -U compinit
compinit

setopt PROMPT_SUBST
setopt autocd

path=(~/.local/bin $path)
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=2000
PROMPT='%F{cyan}[%j] %f%F{red}%(?..%? )%f%B%#%b '
RPROMPT='$(gitprompt-rs zsh) %B%40<..<%~%<< %b'

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source <(qshell completion zsh)
source <(kubectl completion zsh)

bindkey -v
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
bindkey '^f' autosuggest-accept

alias wip='ip address show wlan0 | grep "inet " | cut -d " " -f 6 | cut -d "/" -f 1'
alias startx='ssh-agent startx'
alias ssh='TERM=xterm-256color && ssh'
alias s='sudo '
alias r='proxychains '
alias p='pacman'
alias v='nvim'
alias x='xclip -r -selection c'
alias g='git'
alias k='kubectl'
alias t='trans'
alias l='ls --color'
alias la='l -a'
alias ll='l -l'
alias lla='la -l'
alias grep='grep --color=auto'
alias tree='tree -C'
alias cp='cp -r'
alias scp='scp -r'
alias sctl='systemctl'
alias ...='../..'
alias ....='../../..'

monitor() {
    if [[ "$1" == "x" ]] {
            xrandr --output DP1 --mode 3840x2160 --output eDP1 --off
    } elif [[ "$1" == "e" ]] {
        xrandr --output eDP1 --mode 2560x1440 --output DP1 --off
    }
}
